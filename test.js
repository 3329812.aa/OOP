class Moment {
    #arr = [];
    #result
    constructor(result) {
      this.#result = result;
    }
    get current() {
      return this.#result;
    }
    set current(newValue) {
      if (this.#result) {
        this.#arr.push(this.#result);
      }
      this.#result = newValue;
    }
     
  
    undo() {
      this.#result = this.#arr.pop();
    }
  }
       
  let res = new Moment();
  
  console.log(res.current);
  